#
# Copyright 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Provide a simple interface for accessing the python api.
"""

try: import simplejson as json
except: import json

try:
   import xbmc
except:
   xbmc = None

class JsonError(IOError):
    """Attempt to find the error in returned messages (work in progress)"""
    def __init__(self, err, query):
        if type(err) is str:
            IOError.__init__(self, err + ": %s" % query)
        else: 
            if 'data' in err:
                if 'stack' in err['data'] and 'message' in err['data']['stack']:
                    stack = err['data']['stack']
                    msg = "%s %s(%s)" % (stack['message'], stack['name'], stack['type'])
                else:
                    msg = err['data']['message']
                msg += " @%s" % query
            else:
                msg = query
            IOError.__init__(self, "%s (%d): %s" % (err['message'], err['code'], msg))


class Api(object):
    call_id = 0

    def __init__(self, name=None, parent=None):
        self.parent  = parent
        self.name    = name

    def __getattr__(self, name):
        return Api(name, self)

    def getParent(self):
        if self.parent:
            return self.parent.getParent() + [self]
        return []

    def __str__(self):
        return '.'.join( a.name for a in self.getParent() )

    def __call__(self, **kwargs):
        self.call_id += 1

        query = json.dumps({'jsonrpc':"2.0", 'method': str(self), 'id': self.call_id, 'params': kwargs})
        if not xbmc:
            return query
        res   = xbmc.executeJSONRPC(query)
        try:
            datum = json.loads(res)
        except:
            raise JsonError("Couldn't decode: %s" % res, query)
        if 'result' in datum:
            return datum['result']
        raise JsonError(datum['error'], query)

Xbmc = Api()

if __name__ == '__main__':
    """Some simple testing"""
    a = Api()
    print a
    b = a.CallOne
    print b
    c = b.CallTwo
    print c
    print c(foo=2)
    print c(bar=[1,2,3])
    print a.Simple.Call.Something.Hey(hello='world') 

