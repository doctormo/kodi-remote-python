# -*- coding: utf-8 -*- 
#
# Copyright 2013 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""Cell Render for showing Episode Guides"""

from gi.repository import Gtk, Gdk, GObject

import cairo

CACHE = {}
MODES = [
  (1.0, 0.0, 0.0, 0.5), # 0 Old shows before 3 weeks ago
  (1.0, 1.0, 0.0, 1.0), # 1 Shows between three weks ago and today
  (0.5, 0.5, 0.9, 0.5), # 2 Shows between today and next week (up and coming)
  (0.7, 0.7, 0.7, 0.5), # 3 Shows after next week (far future)

  (0.5, 0.5, 0.5, 1.0), # -2 Specials (season 0)
  (0.8, 0.2, 0.2, 0.5), # -1 No air-date specified (undefined)
]

class GuideCellRenderer(Gtk.CellRenderer):
    data = GObject.Property(
        type=object,
        nick='data',
        blurb='what data to render',
        flags=(GObject.PARAM_READWRITE | GObject.PARAM_CONSTRUCT))

    def do_render(self, cr, widget, bg_area, cell_area, flags):
        context = widget.get_style_context()
        (x, y, w, h) = self.do_get_size(widget, cell_area)
        h -= 4
        y += 2

        context.save()
        context.add_class("background")
        Gtk.render_background(context, cr, x, y, w, h)
        Gtk.render_frame(context, cr, x, y, w, h)
        context.restore()

        context.save()
        context.add_class("foreground")

        m = self.get_property("data").guide.missing()
        if len(m) == 0:
            cr.set_source_rgb(0.1, 0.8, 0.2)
            cr.rectangle(x, y, w, h)
            cr.fill()
            cr.set_source_rgb(0.5,0.5,0.5)
            cr.rectangle(x, y, w, h)
            cr.stroke()
        for s in range(len(m)):
            start = (float(w)/len(m))*s
            width = (float(w)/len(m))*(s+1) - start
            size  = len(m[s])
            #print "Start: %d, Width: %d, Size: %d" % (start, width, size)
            for (a,b,rr) in m[s]:
                cr.set_source_rgba(*MODES[rr])
                pos = start + (width * a)
                block = start + (width * b) - pos
                #print " - from %d to %d (%d)" % (pos, pos+block, block)
                cr.rectangle(x + pos, y, block, h)
                cr.fill()
            if size == 0:
                cr.set_source_rgb(0.1, 0.8, 0.2)
                cr.rectangle(x + start, y, width ,h)
                cr.fill()
            cr.set_source_rgba(0.5, 0.5, 0.5, 1.0)
            cr.rectangle(x + start, y, width ,h)
            cr.stroke()

    def do_get_size(self, widget, cell_area=None):
        """Sets the size the treeview will allocate for us"""
        if cell_area:
            return (cell_area.x, cell_area.y, max(cell_area.width, 200), cell_area.height)
        return (0, 0, 200, 25)

