#!/usr/bin/python

from select import select

import string
import socket
import time
import sys
 
# Telnet port 8102

LOW = 0
HIGH = 161
SET = {
  '25': 'Tv',
  '49': 'Wii',
}
TES = dict( (b,a) for (a,b) in SET.items() )

class Pioneer(object):
    host = '192.168.1.2'
    port = 8102

    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(2)
     
        try:
            self.socket.connect((self.host, self.port))
        except:
            self.socket = None
     
        #print 'Connected to %s:%d' % (self.host, self.port)

    def _vol(self, ret):
        return ((float(ret.replace('VOL','')) + LOW) / HIGH) * 100.0

    def _set(self, ret):
        return SET.get(ret.replace('FN',''), 'Unknown (%s)' % ret)

    def volume_up(self):
        return self._vol(self.command('VU'))

    def volume_down(self):
        return self._vol(self.command('VD'))

    def get_volume(self):
        return self._vol(self.command('?V'))

    def set_volume(self, to):
        to = sorted((0, to, 100))[1]
        volume = int(self.get_volume())
        while volume > to:
            volume = self.volume_down()
        while volume < to: 
            volume = self.volume_up()
        return volume

    def set_to(self, st):
        st = int(TES.get(st, st))
        return self._set(self.command('%dFN' % st))

    def set_to_next(self):
        return self._set(self.command('FU')) # cyclic

    def set_to_prev(self):
        return self._set(self.command('FD')) # cyclic

    def get_setting(self):
        return self._set(self.command('?F'))

    def command(self, command):
        if command is not None:
            self.socket.send(command+'\n\r')
            time.sleep(0.1)

        reads, writes, errors = select([self.socket], [], [])
        if self.socket in reads: 
            data = self.socket.recv(4096)
            if not data:
                print 'Connection closed'
                sys.exit()
            else:
                return data.strip()


if __name__ == "__main__":
    p = Pioneer()
    s = p.get_setting()
    v = p.get_volume()

    print "Volume: %s\nSetting: %s\n" % (v, s)

    if len(sys.argv) > 1:
        (set_to, volume) = sys.argv[1:]
        volume = int(volume)
        if set_to not in SET.values():
            raise IOError("No thing called '%s'" % set_to)

        if set_to != s:
            print p.set_to(set_to)

        if volume != v:
            print p.set_volume(volume)

