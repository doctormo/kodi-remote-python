# -*- coding: utf-8 -*- 
#
# Copyright 2013 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Command-line User Interface using curses while cursing.
"""

import curses
import atexit

import locale
locale.setlocale(locale.LC_ALL, '')
code = locale.getpreferredencoding()

class Screen(object):
    """Control a curses screen in a little more elegance"""
    def __init__(self):
        self.pos = []
        self.label = {}
        self.filled = {}
        self.screen = curses.initscr()
        try:
            atexit.register(self.stop)
            curses.start_color()
            curses.use_default_colors()
            for i in range(0, curses.COLORS):
                curses.init_pair(i + 1, i, -1)

            #curses.cbreak()
            self.screen.keypad(1)
            self.refresh()
        except:
            self.stop()
            raise

    def pushpos(self):
        """Push the current position onto the pos stack"""
        (y, x) = self.screen.getyx()
        self.pos.append((x, y))

    def poppos(self):
        """Pop the last position and reset cursor"""
        (x, y) = (0, 0)
        if self.pos:
            (x, y) = self.pos.pop()
        self.addstr('', x, y)

    def stop(self):
        """End the session and close the curses window"""
        curses.endwin()

    def addstr(self, msg, x, y, length=0, color=None):
        """Print a string to the screen, understands negative coords and length:

        -1 : Unrestricted, flows over the line to the next
        0  : One line maximum, stops at the end of the line
        n  : Limmit to n places

        """
        (b, a) = self.screen.getmaxyx()
        if length <= 0:
            length = (a - x + length)

        msg = msg[:length]

        if x < 0 or y < 0:
            x = x < 0 and a + x - len(msg) or x
            y = y < 0 and b + y or y
        if x > a or y > b or x < 0 or y < 0:
            return None
        if isinstance(msg, unicode):
            try:
                msg = str(msg)
            except UnicodeError:
                msg = msg.encode("utf-8")
        if len(msg) > 0:
            try:
                if color != None:
                    self.screen.addstr(int(y), int(x), msg, curses.color_pair(color))
                else:
                    self.screen.addstr(int(y), int(x), msg)
            except Exception, error:
                #raise IOError("Curses error? (%d, %d) '%s'" % (int(y), int(x), msg))
                pass
        return (int(x), int(y))

    def addlabel(self, name, x, y, length=0):
        """Accociates a label with a name, negative coords and optional
        length (see addstr for details)"""
        self.label[name] = (x, y, length)

    def clear(self, x, y, l):
        self.addstr(" "*l, x, y)

    def printat(self, name, msg):
        if name not in self.label:
            raise KeyError("No label '%s' did you forget to add it?" % name)
        (x, y, length) = self.label[name]
        self.clear(*self.filled.get(name, (0,0,0)))
        nxy = self.addstr(msg, x, y, length)
        if nxy:
            self.filled[name] = (nxy[0], nxy[1], len(msg))

    def print_list(self, start, msg, index=0):
        if start not in self.label:
            raise KeyError("No label '%s' did you forget to add it?" % name)
        (x, y, length) = self.label[start]
        name = "%s%d" % (start, index)
        while name in self.label:
            index += 1
            name = "%s%d" % (start, index)
        self.label[name] = (x, y+index, length)
        self.printat(name, msg)

    def clear_list(self, start, index=0):
        name = "%s%d" % (start, index)
        while name in self.label:
            self.clear(*self.filled.get(name, (0,0,0)))
            self.label.pop(name)
            index += 1
            name = "%s%d" % (start, index)

    def getstr(self, prompt=None, timeout=-1):
        if prompt:
            self.printat(prompt, '')
        (y, x) = self.screen.getyx()
        if timeout is not None:
            self.screen.timeout(timeout)
        ret = self.screen.getstr().strip()
        self.clear(x, y, len(ret))
        return ret

    def refresh(self):
        return self.screen.refresh()

    def wait(self, call_do, call_refresh, quit='q', refresh=700):
        call_refresh()
        key = 0
        while key != ord(quit[0]):
            self.pushpos()
            self.screen.timeout(refresh)
            key = self.screen.getch()
            call_refresh()
            if key > 0:
                call_do(key)
            self.screen.refresh()
            self.poppos()

M = {
 49: 'Foo Bar HaHa',
 50: 'Bring it',
 51: 'Oh what shall we do with our merry go round.',
}

if __name__ == '__main__':
    screen = Screen()

    def key_do(key):
        screen.printat('label', M.get(key, 'Nothing for "%d"' % key))
        screen.printat('volume', '|===========|')
        if key == ord('a'):
            screen.getstr('label')

    def refresh():
        pass

    try:
        screen.addlabel('label', 3, 3, 20)
        screen.addlabel('volume', -2, 0)
        screen.addstr('Hello World? : ', 1, 1)
        screen.wait(key_do, refresh)
    except:
        screen.stop()
        raise
    screen.stop()

