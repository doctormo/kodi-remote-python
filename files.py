#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# Copyright 2012 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Files Guide perhaps.
"""
import os
import sys

from collections import defaultdict

from jsonrpclib.jsonrpc import TransportMixIn, XMLTransport
from jsonrpclib import Server

import logging

class Transport(TransportMixIn, XMLTransport):
    """Replaces the json-rpc mixin so we can specify the http headers."""
    def send_content(self, connection, request_body):
        connection.putheader("Content-Type", "application/json")
        connection.putheader("Content-Length", str(len(request_body)))
        connection.endheaders()
        if request_body:
            connection.send(request_body)

class fset(set):
    def __init__(self, items):
        self.update(items)

    def update(self, items):
        for item in items:
            self.add(item)

    def __str__(self):
        return "\n".join(self.strlist())

    def strlist(self):
        for x in self:
            yield unicode(x.encode('utf-8'), 'utf-8')

IGNORE = set(['idents', 'weekly', '.unfinished', '.unsorted', 'Unsorted', 'Comics', 'Radio'])

class Files(fset):
    def __init__(self, fn='files'):
        with open(fn, 'r') as fhl:
            self.update( fhl.readlines() )

    def add(self, item):
        try:
            item = unicode(item.strip(), 'utf-8')
        except UnicodeDecodeError:
            sys.stderr.write("Oh fuck it python: %s\n" % item)
        
        if IGNORE.intersection(set(item.split('/'))) \
          or item.split('.')[-1] in ['jpg', 'jpeg', 'nfo', 'yaml', 'template']:
            return
        return set.add(self, item)
            

class Shows(fset):
    def __init__(self, user, pwd, host='localhost', port=8080):
        http = "http://%s:%s@%s:%s/jsonrpc" % (user, pwd, host, str(port))
        self.server = Server(http, transport=Transport(), encoding='utf-8')
        self.info = self.server.Application.GetProperties(['name', 'version'])
        for (showid, title) in self.shows():
            for (season, episode, path) in self.episodes(showid):
                self.add(path)
        for (title, path) in self.movies():
            self.add(path)

    def __str__(self):
        return "Connected: %s (%s.%s)" % (self.info['name'],
            self.info['version']['major'], self.info['version']['minor'])

    def add(self, path):
        if 'Internet' in path.split('/'):
            return
        path = path.replace('/media', '/mnt')
        return set.add(self, path)

    def call(self, cmd, *args, **kwargs):
        try:
            if not '.' in cmd:
                raise ValueError([-1, "Invalid Command"])
            (a, b) = cmd.split('.')
            module = getattr(self.server, a)
            return getattr(module, b)(*args, **kwargs)
        except Exception, error:
            raise

    def shows(self):
        result = self.call('VideoLibrary.GetTVShows', ['title'])
        for r in result.get('tvshows'):
            yield (r['tvshowid'], r['title'])

    def movies(self):
        result = self.call('VideoLibrary.GetMovies', ['title','file'])
        for r in result.get('movies'):
            yield (r['title'], r['file'])

    def episodes(self, showid):
        results = self.call('VideoLibrary.GetEpisodes', tvshowid=showid, properties=['episode', 'season', 'file'])
        for e in results.get('episodes', []):
            yield (str(e['season']), str(e['episode']), e['file'])



def diff(A, B):
    return (fset(set(A)-set(B)), fset(set(B)-set(A)))



if __name__ == '__main__':
    (A,B) = diff(Files(), Shows('Tv', '12345678', 'teletabby'))
    import codecs
    fhl = codecs.open('diff', encoding='utf-8', mode='w+')
    fhl.write("======================\n + ")
    fhl.write("\n + ".join(sorted(A)))
    fhl.write("\n======================\n")
    fhl.write("\n - ".join(sorted(B)))
    fhl.close()


