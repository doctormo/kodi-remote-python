#!pythonenv/bin/python
# -*- coding: utf-8 -*- 

from jsonrpclib.jsonrpc import TransportMixIn, XMLTransport
from jsonrpclib import Server

import re
import sys

import procname
procname.setprocname('commandcontrol')

sys.stdout.write("\x1b]2;Remote Control\x07")

sys.path.insert(0, 'lib')

from cui import Screen

from datetime import timedelta
import socket
import locale
locale.setlocale(locale.LC_ALL, '')
code = locale.getpreferredencoding()

NAVIGATOR = {
  10  : 'Input.Select',
  258 : 'Input.Down',
  259 : 'Input.Up',
  260 : 'Input.Left',
  261 : 'Input.Right',
  263 : 'back',
  338 : 'pageup',
  339 : 'pagedown',
  ord(' ') : 'fullscreen',
  ord('f') : 'fullscreen',
  ord('c') : 'VideoLibrary.Clean',
  ord('s') : 'VideoLibrary.Scan',
  ord('m') : 'Input.ContextMenu',
}
PLAYER = {
  ord('0'): 'number0',
  ord('1'): 'number1',
  ord('2'): 'number2',
  ord('3'): 'number3',
  ord('4'): 'number4',
  ord('5'): 'number5',
  ord('6'): 'number6',
  ord('7'): 'number7',
  ord('8'): 'number8',
  ord('9'): 'number9',
  ord('\\'): 'stop',
  263 : 'back',
  ord(' ') : 'playpause',
  10 : 'Input.Select',
  258 : 'Input.Down',
  259 : 'Input.Up',
  260 : 'Input.Left',
  261 : 'Input.Right',
  338 : 'audionextlanguage',
  ord('{') : 'bigstepback',
  ord('[') : 'stepback',
  ord(']') : 'stepforward',
  ord('}') : 'bigstepforward',
  ord('f') : 'fullscreen',
  ord('i') : 'info',
  ord('+') : 'zoomin',
  ord('-') : 'zoomout',
  ord('c') : 'codecinfo',
  ord('s') : 'showsubtitles',
  ord('_') : 'aspectratio',
  ord('n') : 'skipnext',
  ord('b') : 'skipprevious',
}

BUTTONS = {
  10  : [ 3, 3, u'◊'],
  258 : [ 4, 3, u'▾'],
  259 : [ 2, 3, u'▴'],
  260 : [ 3, 1, u'◂'],
  261 : [ 3, 5, u'▸'],
  -1  : [ -2, 2, u'?'],
}
SPEEDS = {
  -2 : u"◂ ",
  -1 : u"◃ ",
   0 : u"• ",
   1 : u"▹ ",
   2 : u"▸ ",
}

            #for i in p['addons']:
            #    if 'ps' in i['addonid']:
            #        self.screen.printat('warn', "$ %s" % str(i['addonid']))
            #p = self.server.Addons.GetAddonDetails('script.pseudotv', ['name', 'extrainfo'])
            #self.screen.printat('warn', "$ %s" % str(p))

class Transport(TransportMixIn, XMLTransport):
    """Replaces the json-rpc mixin so we can specify the http headers."""
    def send_content(self, connection, request_body):
        connection.putheader("Content-Type", "application/json")
        connection.putheader("Content-Length", str(len(request_body)))
        connection.endheaders()
        if request_body:
            connection.send(request_body)

def otime(time):
    result = float(time.get('seconds', 0)) + 1
    result += time.get('minutes', 0) * 60
    result += time.get('hours', 0) * 60 * 60
    result += float(time.get('milliseconds', 0)) / 100
    return result

def ttime(time1, time2):
    return timedelta(0, int(otime(time2))) - timedelta(0, int(otime(time1)))

class ConnectionError(IOError):
    pass

class CliControl(object):
    """A command line control module"""
    def __init__(self, host, user='user', pas='', port='8080'):
        self.pid = None
        self.gui = None
        self.olditm = None
        self.status = (0, 'off')

        self.host = host
        self.http = "http://%s:%s@%s:%s/jsonrpc" % (user, pas, self.host, str(port))
        self._server = Server(self.http, transport=Transport(), encoding='utf-8')
        self.name = 'No Connection'
        self.ver  = ''

        self.screen = Screen()
        self.screen.addlabel('server', 2, 0)
        self.screen.addlabel('volume', -1, 0)
        self.screen.addlabel('status', 2, -2)
        self.screen.addlabel('warn', 2, -3)
        self.screen.addlabel('prog', -1, -2)
        self.screen.addlabel('line', 2, 6)
        self.screen.addlabel('url', -3, -4)
        self.screen.addlabel('urls', 2, 8, -1)

    @property
    def server(self):
        if not self.ver:
            try:
                prop = self._server.Application.GetProperties(['name', 'version'])
                vers = prop['version']
                self.name = prop['name']
                self.ver  = "%s.%s" % (vers['major'], vers['minor'])
            except socket.error:
                raise ConnectionError("Host '%s' is not available." % self.host)
        return self._server

    def call(self, cmd, *args):
        try:
            if not '.' in cmd:
                raise ValueError([-1, "Invalid Command"])
            (a, b) = cmd.split('.')
            module = getattr(self.server, a)
            return getattr(module, b)(*args)
        except Exception, error:
            return str(list(error.message)[1])

    def line(self, s):
        self.screen.printat('line', s)

    def getstr(self, prompt):
        self.line(prompt)
        return self.screen.getstr()

    def set_pad(self, item=None):
        (speed, repeat) = self.status
        BUTTONS[-1][2] = SPEEDS.get(speed, '?')

        for (key, p) in BUTTONS.iteritems():
            (y, x, n) = p
            if item == key:
                self.screen.addstr(n, x, y, color=1 )
            else:
                self.screen.addstr(n, x, y)

    def get_channels(self):
        """Trys to get channel information from psudotv"""
        #self.server.Addons.GetAddonDetails()

    def set_volume(self, delta=0):
        """Set's the volume and prints the status to the screen"""
        old = self.server.Application.GetProperties(['volume'])['volume']
        volume = max(0, min(100, old + delta))
        if delta != 0:
            self.server.Application.SetVolume(volume)
            self.server.Application.SetMute(False)
        p = int(volume / 5)
        c = int(volume % 5)
        self.screen.printat('volume', u"♬ %s%s%s" % (
            u"█" * p,
            (u"", u"▎", u"▌", u"▊", u"▉")[c],
            u" " * (20 - p - int(c != 0))))

    PPS = u" ▁▂▃▄▅▆▇█-"

    def set_progress(self, r, tl):
        l = 30
        p = int(r * l * 8) % l
        if int(r * 8) + 1 < len(self.PPS):
            a = self.PPS[int(r * 8)]
            b = self.PPS[int(r * 8) + 1]
            self.screen.printat('prog', u" %s ⌚ ▕%s%s" % (tl, b * p, a * (l - p)))

    def set_display(self):
        """Get detils about playing item and show"""
        players = self.server.Player.GetActivePlayers()
        window  = self.server.GUI.GetProperties(['currentwindow'])
        if players:
            self.pid = players[0]['playerid']
            self.gui = window['currentwindow']['id']
            self.label = window['currentwindow']['label']
            self.screen.printat('warn', self.label)
            try:
                status = self.server.Player.GetProperties(self.pid,
                    ['repeat', 'type', 'time', 'totaltime', 'speed'])
                played = self.server.Player.GetItem(self.pid,
                    ['title', 'showtitle', 'theme', 'plot', 'thumbnail'])['item']
            except Exception, error:
                self.screen.printat('status', " !- ERROR: %s" % str(error))
                return
            self.status = ( status['speed'], status['repeat'] )

            progress = otime(status['time']) / otime(status['totaltime'])
            timeleft = ttime(status['time'], status['totaltime'])

            label = clean(played['label']).split('//')
            episode = len(label) > 1 and label[1] or ''

            self.screen.printat('status', " ?- %s: %s" % (
                                clean(played.get('showtitle', label[0])),
                                clean(played.get('title', episode)) or episode))
            self.set_progress(progress, timeleft)

            self.set_youtube(**played)
        else:
            self.status = (0, 'off')
            self.screen.printat('status', " -- Playing: Nothing")
            self.screen.printat('prog', "")

    def set_youtube(self, thumbnail='', plot='', **played):
        self.screen.printat('url', '')
        self.screen.clear_list('urls')
        url = thumbnail.split('%2f')

        if 'vi' not in url:
            return

        v = url[url.index('vi')+1]
        self.screen.printat('url', "https://www.youtube.com/watch?v=%s" % v)

        plot = "\n" + plot.split(']')[-1]
        urls = [('None',[])]
        for (label, url) in re.findall('\n([^\n]*\w?[^\n]*)\W*(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+)', plot):
            if label == '':
                urls[-1][1].append(url)
            elif '://' in label:
                urls[-1][1].append(label)
                urls[-1][1].append(url)
            else:
                urls.append( (label, [url]) )

        self.screen.print_list('urls', "[ Doobly Do ]")
        self.screen.print_list('urls', "")
        for (label, urls) in [ (l, u) for (l, u) in urls if u ]:
            for url in urls:
                if any(x in url for x in ['twitter', 'facebook', 'tumblr']):
                    continue
                self.screen.print_list('urls', "%s: %s" % (label[:30].replace(':','').strip(), url))

    def refresh(self):
        self.screen.clear_list('urls')
        self.screen.printat('server', " -- Connected to %s (%s %s) -- " % \
            (self.host, self.name, self.ver))
        self.set_volume()
        self.set_display()
        self.set_pad()
        self.screen.printat('line', "$ ")

    def start(self):
        self.screen.wait(self.do, self.refresh)

    def stop(self):
        self.screen.stop()

    def do(self, key):
        self.set_pad(key)
        playing = self.server.GUI.GetProperties(['fullscreen'])['fullscreen']
        keys = playing and PLAYER or NAVIGATOR
        if keys.has_key(key) and keys[key] == keys[key].lower():
            self.line("$ Action:%s -> %s" % (keys[key],
                self.call("Input.ExecuteAction", keys[key])))
        elif keys.has_key(key):
            self.line("$ %s -> %s" % (keys[key], self.call(keys[key])))
        elif key == 124: # Pipe |
            result = self.getstr('$ ')
            self.line("$ SendText(\"%s\") -> %s" % (result,
                self.call("Input.SendText", result)))
        elif key == 35: # Hash #
            rs = self.getstr('# ').split(',')
            cl = ', '.join(rs[1:])
            self.line("# %s(%s) -> %s" % (rs[0], cl, self.call(*rs)))
        elif key == ord('^'):
            result = self.call('VideoLibrary.GetTVShows', ['title','studio'])
            shows = {}
            channels = {}
            for r in result['tvshows']:
                channel = clean(r['studio'][0])
                shows[r['tvshowid']] = [ clean(r['title']), channel ]
                channels[channel] = channels.get(channel, 0) + 1
            with open('a', 'w') as fhl:
                fhl.write(unicode(channels))
                fhl.write(unicode(shows))
        elif key == ord('g'):
            result = self.getstr('url: ')
            uri = None
            name = None
            if 'youtube' in result:
                name = 'YouTube'
                yid = get_property(result, 'v')
                if yid:
                    uri = {'file': "plugin://plugin.video.youtube/?action=play_video&videoid=%s" % yid}
            if uri and name:
                self.line("# Player.Open(%s) -> %s" % (name, self.call("Player.Open", uri)))
            elif not uri:
                self.line("! Player.Open(%s) -> Can't find ID" % name)
            else:
                self.line("! Player.Open(Unknown) -> Unknown Video")
        elif key == 60 or key == 62:
            self.set_volume((key-61)*3)
        elif key == 410:
            pass
        elif key > 1:
            import curses
            self.line("! Key %s (%s) does nothing." % \
                (curses.keyname(key), str(key)))

def clean(string):
    try:
		return string.encode('utf-8')
    except UnicodeDecodeError:
        return string.decode('utf-8')
    return unicode(string)

def get_property(uri, name):
    """Returns the value of the porperty named"""
    for e in uri.split('?')[-1].split('&'):
        (n, v) = e.split('=', 1)
        if n == name:
            return v
    return None


if __name__ == '__main__':
    HOSTS = ['teletabby', '192.168.1.7']
    remote = CliControl(HOSTS.pop(), 'Tv', '12345678')
    #remote = CliControl('192.168.1.208', 'xbmc', 'eog', port='80')
    if not remote:
        sys.exit(2)
    try:
        remote.start()
    except:
        remote.stop()
        raise
    else:
        remote.stop()
