#!/usr/bin/python

import os
import sys


files = []
season = None
lowest = None
for f in sys.argv[1:]:
    if f[0] != 'S' or f[3] != 'E':
        print "File %s is not in the right format" % f
        sys.exit(2)
    (se, ep) = (int(f[1:3]), int(f[4:6]))
    if not lowest or ep < lowest:
        lowest = ep
    if season == None:
        season = se
    elif season != se:
        print "File %s, not in season %d (Exiting)" % (f, season)
        sys.exit(2)
    files.append((f, ep, sxex[6:]))

for (f, ep, rest) in files:
    ep -= lowest + 1
    nf = 'S%dE%d%s' % (season, ep, rest)
    print "Renaming %s -> %s" % (f, nf)
    #os.rename(f, nf )

