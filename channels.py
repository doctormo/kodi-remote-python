#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# Copyright 2012 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Graphical xbmc remote perhaps.
"""

from jsonrpclib.jsonrpc import TransportMixIn, XMLTransport
from jsonrpclib import Server

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')

from gtkme import Window, GtkApp, TreeView, ViewColumn, ViewSort, PixmapManager
from gi.repository import Gtk, Gdk

import os
import sys
import json
import hashlib
import logging

HOST = len(sys.argv) > 1 and sys.argv[-1] or 'tv'
USER = 'Tv'
PASS = '12345678'

DEBUG = False #True
DEBUG_PROP = { 'version': {'major':'-1','minor':'0'}, 'name':'Debug Mode' }
ART = { 'banner': 'image://pixmaps/shows/show.png' }
DEBUG_DATA = { 'tvshows':[
  { 'tvshowid': 'a', 'title': 'Debug Show A', 'genre':['Debug'], 'studio':['BBC One'], 'plot':'Desc1', 'art': ART },
  { 'tvshowid': 'b', 'title': 'Debug Show B', 'genre':['Debug'], 'studio':['BBC One'], 'plot':'Desc2', 'art': ART },
  { 'tvshowid': 'c', 'title': 'Debug Show C', 'genre':['Debug'], 'studio':['BBC Four'],'plot':'Desc3', 'art': ART },
]}
DEBUG_GUIDE = {
  "1": { "1": ["Nothing But the Truth", "1995-01-03"],
         "2": ["Heartland", "1995-01-10"],
         "3": ["A Family Affair", "1995-01-17"],
         "4": ["The Sweetest Thing", "1995-01-24"]},
  "3": { "1": ["Mute of Malice", "1997-03-03"],
         "2": ["Blood Money", "1997-03-10"],
         "3": ["Ancient History", "1997-03-17"]},
  "2": { "9": ["Nothing But the Truth", "1995-01-03"], # Past
         "10": ["Heartland", "2013-03-20"], # Near Past
         "11": ["A Family Affair", "2013-03-27"], # Present
         "12": ["The Sweetest Thing", "2014-02-24"]}, # Future
}
DEBUG_EPISODES = {
  'a' : [ { 'episode': 1, 'season': 1, 'file': '' },
          { 'episode': 2, 'season': 1, 'file': '' },
          { 'episode': 3, 'season': 1, 'file': '' },
          { 'episode': 4, 'season': 1, 'file': '' } ],
  'b' : [
          { 'episode': 1, 'season': 3, 'file': '' },
          { 'episode': 2, 'season': 3, 'file': '' },
          { 'episode': 3, 'season': 3, 'file': '' },
          { 'episode': 3, 'season': 1, 'file': '' },
        ],
  'c' : [
          { 'episode': 3, 'season': 1, 'file': '' },
        ],
}

from xmlbase import EpisodeGuide
from lxml import etree
from cellrenderers import GuideCellRenderer

from gtkme.pixmap import SizeFilter, OverlayFilter, InternetPixmaps

# Channel record saves all the channels moved around
CHRECORD = {}
CHFILE = 'record.json'
EXTRA_CHANNELS = ['Alibi', 'SciFi Channel', 'Anime Network']

def label(s):
    return s.replace('&amp;','&').replace('&', '&amp;')

if os.path.exists(CHFILE):
    with open(CHFILE) as fhl:
        CHRECORD = json.loads(fhl.read())

class WebImages(InternetPixmaps):
    pixmap_dir = './pixmaps'
    filters = [ SizeFilter ]


class Transport(TransportMixIn, XMLTransport):
    """Replaces the json-rpc mixin so we can specify the http headers."""
    def send_content(self, connection, request_body):
        connection.putheader("Content-Type", "application/json")
        connection.putheader("Content-Length", str(len(request_body)))
        connection.endheaders()
        if request_body:
            connection.send(request_body)

class Season(object):
    def __init__(self, number):
        self.season = number
        self.episodes = []

    def add(self, episode):
        self.episodes.append(episode)

    @property
    def all_missing(self):
        for e in self.episodes:
            if not e.missing:
                return False
        return True

    @property
    def any_missing(self):
        for e in self.episodes:
            
            if e.missing:
                return True
        return False

    def __unicode__(self):
        if self.all_missing:
            return "<b>%d</b>" % int(self.season)
        elif self.any_missing:
            return "<i>%d</i>" % int(self.season)
        return "%d" % int(self.season)

class Episode(object):
    def __init__(self, number, missing, files, name, aired):
        self.missing = missing
        self.files = files
        self.episode = number
        self.name = "%s - %s (%s)" % (number, name, aired)

    def __unicode__(self):
        if self.missing:
            return u"<b>☢ %s</b>" % unicode(self.name)
        if self.files and isinstance(self.files, list):
            return u"<i>* %s</i>" % unicode(self.name)
        return self.name

import urllib
def clean_img(u):
    if u and 'image://' in u:
        return urllib.unquote(u[8:-1])
    return 'default.png'

def without(a, *p):
    for ab in a:
        if ab.lower() not in p:
            yield ab

class Show(object):
    #icon = 'show'
    def __init__(self, d):
        self.chobj    = None
        self.tvid     = d['tvshowid']

        if d['genre']:
            self.genres   = clean(d['genre'][0]).replace('Action and Adventure', 'Action')\
                .replace('Science-Fiction', 'SciFi').replace('Game Show', 'Quiz').split(' / ')
        else:
            self.genres   = []
        self.genre    = label(", ".join(self.genres))
        self.name     = label(clean(d['title']))
        self.desc     = label(clean(d['plot']))
        self.art      = clean_img(d['art'].get('banner', d['art'].get('fanart', 'show.png')))
        self.channel  = label(clean((d.get('studio', ['Unknown']) or ['None'])[0]))
        self.episodes = int(d.get('episode', 0))
        self.icon     = "-".join(without(self.genres, 'special interest', 'mini-series', 'reality')).lower()

        myid = hashlib.md5()
        myid.update(d['title'].encode('utf-8'))
        self.id = myid.hexdigest()

        self.guide = EpisodeGuide(None)
        if d.has_key('episodeguide'):
            self.guide = EpisodeGuide(d['episodeguide'])
        elif DEBUG:
            self.guide.update(DEBUG_GUIDE)

    def set_channel(self, c):
        self.chobj = c

    def __unicode__(self):
        return self.name


class Channel(object):
    def __init__(self, name):
        self.name = name
        self.episodes = 0
        self.shows = []

    def add_show(self, show):
        if show.chobj:
            show.chobj.shows.remove(show)
            show.chobj.episodes -= show.episodes
        show.set_channel(self)
        self.shows.append(show)
        self.episodes += show.episodes

    def icon(self):
        return unicode(self.name).lower().replace(' ', '-') or 'default'

    def __eq__(self, name):
        return self.name == name

    def __unicode__(self):
        return self.name


class ShowTreeView(TreeView):
    def setup(self, svlist):
        """Setup the treeview with one or many columns"""
        self.ViewColumn('TV Show', expand=True, text='name', icon='icon', pad=6,
            pixmaps=PixmapManager('tvshows', pixmap_dir='./pixmaps', size=16),
          )
        self.ViewColumn('Guide', renderer=GuideCellRenderer)
        self.ViewColumn('Genre', expand=True, text='genre')


class ChannelTreeView(TreeView):
    """Show how easy it is to make a list of items with icons."""
    def get_show_count(self, item):
        return len(item.shows)

    def setup(self, svlist):
        """Setup the treeview with one or many columns"""
        self.ViewColumn('Channel', expand=True,
            text='name', template="<b>%s</b>", icon='icon', pad=6,
            pixmaps=PixmapManager('channels', pixmap_dir='./pixmaps', size=64),
          )
        self.ViewColumn('Shows', expand=True, text=lambda item: len(item.shows))
        self.ViewColumn('Episodes', expand=False, text='episodes')
        # Sort can sort by comparing column values, or give priority to terms.
        self.sort = self.ViewSort(data=lambda item: len(item.shows), ascending=True)

def sort_episodes(item):
    if isinstance(item, Season):
        return int(item.season)
    return int(item.episode)


class RenamerWindow(Window):
    name = 'renamer'

    def __init__(self, show, *args, **kwargs):
        Window.__init__(self, *args, **kwargs)
        self.show = show


class ControlWindow(Window):
    name = 'channels'

    def __init__(self, *args, **kwargs):
        Window.__init__(self, *args, **kwargs)
        self.images = WebImages('inet', size=256)
        self.host = HOST
        self.http = "http://%s:%s@%s:8080/jsonrpc" % (USER, PASS, HOST)
        self.server = Server(self.http, transport=Transport(), encoding='utf-8')
 
        #print self.call("Files.GetSources", media="files")
        #print self.call("Files.GetDirectory", directory="/media/Deimos/idents/cbeebies/", media="files")
        #exit(1)

        if DEBUG:
            prop = DEBUG_PROP
        else:
            prop = self.server.Application.GetProperties(['name', 'version'])
        vers = prop['version']
        self.name = prop['name']
        self.ver  = "%s.%s" % (vers['major'], vers['minor'])

        self.channels = ChannelTreeView(self.widget('channelist'), selected=self.flip)
        self.shows    = ShowTreeView(self.widget('shows'), selected=self.showshow)
        self.episodes = TreeView(self.widget('episodes'), sort=sort_episodes)

        self.shows._list.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)
        self._channel_cache = self.get_shows()
        self.channels.sort.resort()

        TARGET = [("text/plain", Gtk.TargetFlags.OTHER_WIDGET, 0)]
        self.shows._list.enable_model_drag_source( Gdk.ModifierType.BUTTON1_MASK,
                                                TARGET, Gdk.DragAction.MOVE)
        self.channels._list.connect('drag-data-received', self.showmove)
        self.channels._list.enable_model_drag_dest(TARGET, Gdk.DragAction.MOVE)

        self.window.show_all()

    def showmove(self, treeview, context, x, y, data, info, etime):
        drow = treeview.get_dest_row_at_pos(x, y)
        if drow != None:
            tpath, dpos = drow
            channel = self.channels.get_item(tpath)
            shows = self.shows.selected_items()
            for show in shows:
                if self._set_channel(show, channel.name):
                    channel.add_show(show)
                else:
                    sys.stderr.write("Show not updated!! %s -> %s\n" % (show.name, result))
            context.finish(success=True, del_=True, time=etime)
        context.finish(success=False, del_=False, time=etime)

    def _set_channel(self, show, channel):
        print " -> Moving %s from '%s' to '%s'" % (unicode(show), show.channel, channel)
        global CHRECORD
        if not DEBUG:
            result = self.call('VideoLibrary.SetTVShowDetails', tvshowid=show.tvid, studio=[channel])
        else:
            result = 'OK'
        if result == 'OK':
            CHRECORD[show.id] = channel
            show.channel = channel
        return result == 'OK'

    def flip(self, item):
        self.shows.clear()
        self.shows.add(item.shows)
        for show in item.shows:
            self.get_episodes(show)
            self.showshow(show)

    def showshow(self, item):
        guide = item.guide
        am = item.guide.interference(item.guide.marks)
        fk = item.guide.marks
        self.episodes.clear()
        for season in guide.keys():
            soca = Season(season)
            parent = self.episodes.add_item(soca)
            for episode in guide[season].keys():
                missing = am.get(season, {}).get(episode, False)
                files = fk.get(season, {}).get(episode, None)
                episode = Episode(episode, missing, files, *guide[season][episode])
                self.episodes.add_item(episode, parent)
                soca.add(episode)
                if isinstance(files, list):
                    print "Dupes: %s" % unicode(files)
        self.widget("showname").set_markup("<b>%s</b>" % item.name)
        self.widget("showdesc").set_markup("<i>%s</i>" % item.desc[:200])
        if item.art:
            self.widget("showimg").set_from_pixbuf(self.images.get(item.art))
        self.episodes.sort.resort()

    def refreshshow(self, widget):
        show = self.shows.selected
        if show:
            show.guide.refresh()
            self.showshow(show)
            row = self.shows.get_row(show)
            self.shows._model.emit("row_changed", row.path, row.iter); 

    def get_shows(self):
        self._show_cache = list(self._shows())
        channels = {}
        for show in self._show_cache:
            if CHRECORD.get(show.id, show.channel) != show.channel:
                self._set_channel(show, CHRECORD[show.id])
            elif CHRECORD.has_key(show.id):
                print "Show is correct: %s(%s) %s==%s" % (unicode(show), show.id, show.channel, CHRECORD[show.id])
            if show.channel not in channels:
                channels[show.channel] = Channel(show.channel)

        for channel in EXTRA_CHANNELS:
            if channel not in channels:
                channels[channel] = Channel(channel)

        self.channels.add(channels.values())
        for show in self._show_cache:
            channels[show.channel].add_show(show)
        return channels

    def _shows(self):
        if DEBUG:
            result = DEBUG_DATA
        else:
            result = self.call('VideoLibrary.GetTVShows', ['title','studio','episode','genre','episodeguide','plot','art'])
        for x, r in enumerate(result.get('tvshows')):
            sys.stdout.write("Getting show: %d (%s)        \r" % (x, r.get('title', 'Unknown')))
            yield Show(r)

    def get_episodes(self, show):
        if DEBUG:
            results = {'episodes': DEBUG_EPISODES[show.tvid]}
        else:
            results = self.call('VideoLibrary.GetEpisodes', tvshowid=show.tvid, properties=['episode', 'season', 'file'])
        for e in results.get('episodes', []):
            #print clean(e['file'])
            show.guide.account(str(e['season']), str(e['episode']), clean(e['file']))

    def call(self, cmd, *args, **kwargs):
        try:
            if not '.' in cmd:
                raise ValueError([-1, "Invalid Command"])
            (a, b) = cmd.split('.')
            module = getattr(self.server, a)
            return getattr(module, b)(*args, **kwargs)
        except Exception, error:
            raise
            return str(list(error.message)[1])

    def select_show(self, show):
        print " - %s %d" % (show.name, show.tvid)
        self.channels.set_selected(self._channel_cache[show.channel])
        self.shows.set_selected(show)

    def search(self, widget=None):
        text = widget.get_text()
        for show in self._show_cache:
            if text.lower() in show.name.lower():
                return self.select_show(show)

    def renamer(self, widget=None):
        RenamerWindow(self.shows.selected, parent=self)
        


def clean(string):
    return unicode(string)

class ControlApp(GtkApp):
    glade_dir = './'
    app_name = 'xbmccontrol'
    windows = [ ControlWindow, RenamerWindow ]


if __name__ == '__main__':
    try:
        app = ControlApp(start_loop=True)
    except KeyboardInterrupt:
        logging.info("User Interputed")
    with open(CHFILE, 'w') as fhl:
        fhl.write(json.dumps(CHRECORD))
    logging.debug("Exiting Application")

