#
# Copyright (c) 2011, Martin Owens <doctormo@ubuntu.com>
#
# This Program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This Program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the project; see the file COPYING.  If not, write to
# the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
# http://www.gnu.org/copyleft/gpl.html
#
"""Provides some simple xml and other common functions."""

import os
import re
import sys
import json
import urllib2
import logging

from xml.dom.minidom import parseString, NodeList, Node
from xml.parsers.expat import ExpatError
from datetime import date, timedelta
from zipfile import ZipFile

def cleanValue(value):
    """Returns a naturalised value by guessing on it's type"""
    try:
        if '.' in value:
            return float(value)
        else:
            return int(value)
    except ValueError:
        pass
    if value == 'true':
        return True
    if value == 'false':
        return False
    return unicode(value)

def dumpXml(xml, error=None):
    """Saves xml to a standard place"""
    if isinstance(xml, NodeList):
        xml = xml[0].parentNode
    if isinstance(xml, Node):
        xml = xml.toxml()
    if xml == None:
        return logging.error("No XML returned: None.")
    if not isinstance(xml, basestring):
        return logging.error("Can't save xml *shrug* not xml: (%s)" % (type(xml).__name__))
    lines = xml.split('\n')
    if error:
        # Insert a findable string at the right location
        (line, col) = (error.lineno-1, error.offset)
        lines[line] = lines[line][:col] + ' *** HERE *** ' + lines[line][col:]
        xml = '\n'.join(lines)
    filename = '/tmp/xml_parser.log'
    fhl = open(filename, 'w')
    fhl.write(xml.replace('><', ">\n<"))
    fhl.close()
    return filename


def cleanXml(xml, **options):
    """Using xml.minidom we are going to try an make a data structure from xml"""
    try:
        return cleanTags(parseString(xml).childNodes, **options)
    except ExpatError, error:
        filename = dumpXml(xml, error)
        raise ExpatError("Can't parse xml, see %s" % filename)

def cleanTags(nodelist, lists=None, protect=None):
    """Takes the soapy stones and returns a data structure (step 2)"""
    result = None
    listing = False
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE and node.data.strip():
            # Only fall to string if we have some content.
            value = cleanValue(node.data)
            if isinstance(result, dict):
                result['data'] = value
            elif isinstance(result, basestring):
                result += value
            else:
                result = value
        elif node.nodeType == node.ELEMENT_NODE:
            # What happens when we're in trouble.
            if result != None and not isinstance(result, dict):
                result += node.toxml()
                continue
            if result == None:
                result = {}

            # Assume xml tag list, no attributes
            name = node.tagName
            value = cleanTags(node.childNodes, lists=lists, protect=protect)

            # Collapsing dictionaries
            if isinstance(value, dict):
                if len(value.keys()) == 0:
                    value = None
                elif len(value.keys()) == 1:
                    if not (protect and value.keys()[0] in protect):
                        value = value.values()[0]

            if node.hasAttribute('id'):
                # Looks like a tag value, name/value pair
                name = node.getAttribute('id')
                if node.hasAttribute('value'):
                    value = cleanValue(node.getAttribute('value'))
            elif node.hasAttributes():
                # Looks like an attribute dictionary, wrap tag data
                value = value and { 'data': value } or {}
                # Grab each attribute manually, update doesn't really work
                for key in node.attributes.keys():
                    value[key] = node.getAttribute(key)

            # Using a listing variable instead of detecting lists
            # Prevents multiple depth level collapse.
            if result.has_key(name):
                if listing:
                    result[name].append(value)
                else:
                    result[name] = [ result[name], value ]
                    listing = True
            else:
                if lists and name in lists:
                    # Force this to be a list
                    result[name] = [ value ]
                    listing = True
                else:
                    result[name] = value
    return result

def XmlOrData(data, tag=None, **opts):
    if isinstance(data, basestring):
        if tag and data[:len(tag)+2] != '<%s>' % tag:
            data = "<%s>%s</%s>" % (tag, data, tag)
        data = cleanXml(data, **opts)
    if tag and isinstance(data, dict) and data.has_key(tag):
        data = data[tag]
    return data

def XmlOrList(data, tag=None, **opts):
    data = XmlOrData(data, tag=tag, **opts)
    if isinstance(data, dict):
        data = [ data ]
    return data or []

class ImageUrls(list):
    """Breaks down a thumbnail's XML and gives the user a list of images"""
    def __init__(self, content):
        for entry in XmlOrList(content, 'fanart'):
            self.append(entry)

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return "<List of '%d' Images>" % len(self)

from lxml import etree

class DictMatrix(dict):
    def add(self, datum, *keys):
        d = self
        for key in keys[:-1]:
            if not d.has_key(key):
                d[key] = {}
            d = d[key]
        d[keys[-1]] = datum

    def interference(self, source):
        return _i(self, source)

def _i(a, b):
    """Produces a XOR merge on multi-dimentional dictionary"""
    s = {}
    for key in a.keys():
        r = None
        if b.has_key(key):
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                r = _i(a[key], b[key])
        else:
            r = a[key]
        if r:
            s[key] = r
    for key in b.keys():
        if not a.has_key(key):
            s[key] = b[key]
    return s


class EpisodeGuide(DictMatrix):
    """Breaks down the episode guide xml into usable data"""
    path = os.path.expanduser('~/.cache/xbmc/')

    def __init__(self, guidexml):
        self.marks  = DictMatrix()
        self._mis   = None
        self._min   = {}
        self.cache  = None
        self.url    = None
        if guidexml:
            url = etree.fromstring(guidexml)[0]
            self.cache = url.attrib['cache'].replace('.xml', '.json')
            self.url = url.text

            if not os.path.exists(self.path):
                os.makedirs(self.path)
            self.update(self.get_guide())

    def process_guide(self, xml):
        """Return a dictionary based on guide data"""
        result = DictMatrix()
        data = cleanXml(xml)['Data'].get('Episode', None)
        if not isinstance(data, list):
            return {}
        for epi in data:
            name = unicode(epi['EpisodeName']).strip()
            if name in [None, 'None', 'none', ''] or 'delete' in name:
                continue
            result.add((epi['EpisodeName'], epi['FirstAired']), str(epi['SeasonNumber']), str(epi['EpisodeNumber']))
        return result

    def account(self, season, episode, data):
        """Marks an episode as gotten"""
        conflict = self.marks.get(season, {}).get(episode, None)
        if conflict and isinstance(conflict, list) and data not in conflict:
            conflict.append(data)
        elif conflict and conflict != data:
            conflict = [conflict, data]
        else:
            conflict = data
        self.marks.add(conflict, str(season), str(episode))

    def report(self):
        result = ''
        m = self.interference(self.marks)
        for season in sorted(m.keys(), cmp=lambda a,b: cmp(int(a),int(b))):
            for episode in sorted(m[season].keys(), cmp=lambda a,b: cmp(int(a),int(b))):
                try:
                    result += " - S%02dE%02d - %s\n" % (int(season), int(episode), self[season][episode])
                except KeyError:
                    result += " ! S%02E%02d - MISSING FROM GUIDE\n" % (int(season), int(episode))
        return result

    def missing(self):
        if self._mis == None:
            try:
                self._mis = list(self._missing())
            except IOError, error:
                print "Ignoring missin guide: %s" % str(error)
                return []
        return self._mis

    def lim_episodes(self, season):
        if not self._min.has_key(season):
            keys = [ int(e) for e in self[season].keys() ]
            self._min[season] = float( min( keys ) ) - 1
        return self._min[season], float( len(self[season]) )

    def _missing(self):
        m = self.interference(self.marks)
        for season in sorted(self.keys(), cmp=lambda a,b: cmp(int(a),int(b))):
            if m.has_key(season):
                yield list(self._missing_item(season, m[season].keys()))
            else:
                yield []

    def _missing_item(self, season, keys):
        last_aired = None
        for episode in sorted(keys, cmp=lambda a,b: cmp(int(a),int(b))):
            (min_e, max_e) = self.lim_episodes(season)
            e = float(episode) - min_e # Correct for absurd episodes
            if e < 1:
                print "Episode %s = %d < %d max(%d)" % (episode, e, min_e, max_e)
                continue
            (name, aired) = self.get(season, episode)
            # This logic allows future dates to indicate the next
            # episode is also in the future.
            aired = aired or last_aired
            last_aired = aired
            yield ((e-1) / max_e, e / max_e, self.get_mode( aired, season ))

    threeweeksago = date.today() - timedelta(days=21)
    nextweek = date.today() + timedelta(days=7)
    today = date.today()

    def get(self, season, episode=None):
        """Allow us to get an episode/season too"""
        if episode:
            return dict.get(self, season, {}).get(episode, ('', None))
        return dict.get(self, season)

    def get_mode(self, aired, season):
        """Returns a mode for the aired date and season"""
        if season in [0, '0']:
            return -2
        if aired and '-' in aired:
            aired = date(*[int(a) for a in aired.split('-')])
            if aired < self.threeweeksago:
                return 0
            elif aired < self.today:
                return 1
            elif aired < self.nextweek:
                return 2
            elif aired >= self.nextweek:
                return 3
        return -1

    def paths(self):
        filename = '.'.join(self.url.split('/')[3:])
        return os.path.join(self.path, filename),\
               os.path.join(self.path, self.cache)

    def refresh(self):
        """Deletes cached files and refreshes from the internet"""
        (guidepath, cachepath) = self.paths()
        os.unlink(cachepath) if os.path.exists(cachepath) else None
        os.unlink(guidepath) if os.path.exists(guidepath) else None
        self.update( self.get_guide() )

    def get_guide(self):
        """Downloads cache and returns the xml"""
        # Two levels of cache, one zip file and the other processed.
        (guidepath, cachepath) = self.paths()
        if not os.path.exists(cachepath):
            if not os.path.exists(guidepath):
                try:
                    response = urllib2.urlopen(self.url)
                    logging.debug("Fetched %s" % self.url)
                except urllib2.URLError:
                    logging.debug("Failed %s" % self.url)
                    return None
                xmlfh = open(guidepath, 'wb')
                xmlfh.write(response.read())
                xmlfh.close()
            zipfile = ZipFile(guidepath)
            # Removed with lines for python 2.4 used in xbmc
            # zipfile.open() is new in python 2.6, removed.
            #xmlfh = zipfile.open('en.xml', 'r') 
            data = self.process_guide(zipfile.read('en.xml'))
            #xmlfh.close()
            if data:
                jsonfh = open(cachepath, 'w')
                jsonfh.write(json.dumps(data))
                jsonfh.close()
        else:
            jsonfh = open(cachepath, 'r')
            data = json.loads(jsonfh.read())
            jsonfh.close()
        return data


class PathSettings(dict):
    """Decode and represent xml stored inside a query (yuk!)"""
    # We never really need defs, the format of the xml provides.
    defs = None

    def __init__(self, content):
        data = XmlOrData(content, 'settings')
        if not isinstance(data, dict):
            raise KeyError("XML Settings Data must be dict or xml not %s.\n" % str(type(data)))
        self.update(data)


def pt(n, key):
    print "Key '%s' -(%s)> %s" % (key, type(n[key]).__name__, str(n[key]))
